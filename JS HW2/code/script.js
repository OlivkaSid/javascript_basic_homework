document.write('Ex1'+'<br>');
var tf = 101;
if (tf==10) {
    document.write('Вірно' + '<br>');
} else {
    document.write('Невірно' + '<br>');
};

document.write('<br>'+'<br>'+'Ex2'+'<br>');
var min = 17;
if (min < 0 || min > 59) {
    document.write('Ваш годинник зламався' + '<br>');
} else if (min <= 15) {
    document.write('Перша чверть' + '<br>');
} else if (min <= 30) {
    document.write('Друга чверть' + '<br>');
} else if (min <= 45) {
    document.write('Третя чверть' + '<br>');
} else if ( min <= 59) {
    document.write('Четверта чверть' + '<br>');
}

document.write('<br>'+'<br>'+'Ex3'+'<br>');
var num = 4;
var result;
if (num==1){
    result = 'Зима';
} else if (num==2){
    result = 'Весна';
} else if (num==3){
    result = 'Літо';
} else if (num==4){
    result = 'Осінь';
} else {
    result = 'Помилка!';
};
document.write(result + '<br>');