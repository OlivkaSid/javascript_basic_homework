const wallet = {
    userName: 'Освальд',
    show : function(currency) {
        document.write('Доброго дня, ' + wallet.userName + '! На вашому рахунку' + currency.logo + currency.name + ' залишилось ' + currency.coins + 'одиниць. <br>');
        document.write('Якщо ви продасте їх сьогодні, то отримаєте ' + (currency.coins*currency.price) + ' грн.')
    },
    bitcoin: {
        name: 'Bitcoin',
        logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/1.png" style="width: 20px; padding:0 10px;" alt="bitcoin" />',
        price: 3268.20,
        coins: 3,
    },
    ethereum: {
        name: 'Ethereum',
        logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png" style="width: 20px; padding:0 10px;" alt="bitcoin" />',
        price: 2889.21,
        coins: 10,
    },
    stellar: {
        name: 'Stellar',
        logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/512.png" style="width: 20px; padding:0 10px;" alt="bitcoin" />',
        price: 0.1848,
        coins: 7,
    },
};

wallet.show(wallet.stellar)